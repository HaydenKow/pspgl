#include <string.h>
#include <pspvfpu.h>

#include "pspgl_internal.h"

void glLoadMatrixf (const GLfloat *m)
{
	float temp_mat[4][4] __attribute__((aligned(16)));
	memcpy(temp_mat, m, sizeof(temp_mat));

	struct pspgl_matrix_stack *stk = pspgl_curctx->current_matrix_stack;
	struct pspgl_matrix *mat = pspgl_curctx->current_matrix;

	assert(stk->flags & MF_VFPU);

	pspvfpu_use_matrices(pspgl_curctx->vfpu_context, VFPU_STACKTOP, 0);

	asm volatile("lv.q	c700,  0 + %0\n"
							 "lv.q	c710, 16 + %0\n"
							 "lv.q	c720, 32 + %0\n"
							 "lv.q	c730, 48 + %0\n"
							 :
							 : "m"(*temp_mat)
							 : "memory");

	if (!(stk->flags & MF_DISABLED))
		stk->flags |= MF_DIRTY;

	mat->flags &= ~MF_IDENTITY;
}
