PSPPATH := $(shell psp-config --psp-prefix)
PSPSDK := $(shell psp-config --pspsdk-path)
ARCH = psp-

CC = $(ARCH)gcc -std=gnu99
AR = $(ARCH)ar
RANLIB = $(ARCH)ranlib
RM = rm -f
CFLAGS:= -g -Wall -O2 -G0 -fsingle-precision-constant -I. -Iinclude -I$(PSPPATH)/include -I$(PSPSDK)/include
LFLAGS:= $(CFLAGS)  -G0 -L$(PSPPATH)/lib

.PHONY: release

release: CFLAGS:= $(CFLAGS) -g0 -s
release: $(DEPDIR) $(libGL.a_OBJS_OUT_OUT) $(libGLU.a_OBJS_OUT) $(libglut.a_OBJS_OUT) libGL.a libGLU.a libglut.a

DEPDIR = .deps

API_OBJS = \
	src/eglBindTexImage.o \
	src/eglChooseConfig.o \
	src/eglCreateContext.o \
	src/eglCreatePbufferSurface.o \
	src/eglCreateWindowSurface.o \
	src/eglDestroyContext.o \
	src/eglDestroySurface.o \
	src/eglGetConfigAttrib.o \
	src/eglGetConfigs.o \
	src/eglGetError.o \
	src/eglGetDisplay.o \
	src/eglInitialize.o \
	src/eglMakeCurrent.o \
	src/eglQueryString.o \
	src/eglSwapBuffers.o \
	src/eglSwapInterval.o \
	src/eglTerminate.o \
	src/eglWaitGL.o \
	src/eglWaitNative.o \
	src/glActiveTexture.o \
	src/glAlphaFunc.o \
	src/glArrayElement.o \
	src/glBegin.o \
	src/glDrawBezierArrays.o \
	src/glDrawBezierElements.o \
	src/glDrawSplineArrays.o \
	src/glDrawSplineElements.o \
	src/glBindBufferARB.o \
	src/glBindTexture.o \
	src/glBlendEquation.o \
	src/glBlendFunc.o \
	src/glBufferDataARB.o \
	src/glBufferSubDataARB.o \
	src/glClear.o \
	src/glClearColor.o \
	src/glClearDepth.o \
	src/glClearDepthf.o \
	src/glClearStencil.o \
	src/glColor.o \
	src/glColorMask.o \
	src/glColorPointer.o \
	src/glColorTable.o \
	src/glCompressedTexImage2D.o \
	src/glCopyTexImage2D.o \
	src/glCullFace.o \
	src/glDeleteBuffersARB.o \
	src/glDeleteTextures.o \
	src/glDepthFunc.o \
	src/glDepthMask.o \
	src/glDepthRange.o \
	src/glDepthRangef.o \
	src/glDrawArrays.o \
	src/glDrawBuffer.o \
	src/glDrawElements.o \
	src/glEnable.o \
	src/glEnableClientState.o \
	src/glEnd.o \
	src/glFinish.o \
	src/glFlush.o \
	src/glFog.o \
	src/glFrontFace.o \
	src/glFrustumf.o \
	src/glFrustum.o \
	src/glGenBuffersARB.o \
	src/glGenTextures.o \
	src/glGetBufferSubDataARB.o \
	src/glGetFloatv.o \
	src/glGetIntegerv.o \
	src/glGetError.o \
	src/glGetString.o \
	src/glInterleavedArrays.o \
	src/glIsTexture.o \
	src/glLight.o \
	src/glLightModel.o \
	src/glLineWidth.o \
	src/glLoadIdentity.o \
	src/glLoadMatrixf.o \
	src/glLockArraysEXT.o \
	src/glLogicOp.o \
	src/glMapBufferARB.o \
	src/glMaterial.o \
	src/glMatrixMode.o \
	src/glMultMatrixf.o \
	src/glNormal.o \
	src/glNormald.o \
	src/glNormalPointer.o \
	src/glOrtho.o \
	src/glOrthof.o \
	src/glPixelStore.o \
	src/glPushAttrib.o \
	src/glPushClientAttrib.o \
	src/glPopMatrix.o \
	src/glPushMatrix.o \
	src/glPolygonMode.o \
	src/glPolygonOffset.o \
	src/glPrioritizeTextures.o \
	src/glReadBuffer.o \
	src/glReadPixels.o \
	src/glRotatef.o \
	src/glScalef.o \
	src/glScaled.o \
	src/glScissor.o \
	src/glShadeModel.o \
	src/glStencilFunc.o \
	src/glStencilMask.o \
	src/glStencilOp.o \
	src/glTexCoord.o \
	src/glTexCoordPointer.o \
	src/glTexEnv.o \
	src/glTexGen.o \
	src/glTexImage2D.o \
	src/glTexSubImage2D.o \
	src/glTexParameter.o \
	src/glTranslatef.o \
	src/glUnmapBufferARB.o \
	src/glVertex.o \
	src/glVertexd.o \
	src/glVertexi.o \
	src/glVertexPointer.o \
	src/glViewport.o \
	src/glWeightPointerPSP.o \
	src/pspgl_buffers.o \
	src/pspgl_context.o \
	src/pspgl_copy_pixels.o \
	src/pspgl_dlist.o \
	src/pspgl_ge_init.o \
	src/pspgl_hash.o \
	src/pspgl_matrix.o \
	src/pspgl_misc.o \
	src/pspgl_stats.o \
	src/pspgl_texobj.o \
	src/pspgl_varray.o \
	src/pspgl_varray_draw.o \
	src/pspgl_varray_draw_elts.o \
	src/pspgl_varray_draw_range_elts.o \
	src/pspgl_vidmem.o

libGL.a_OBJS = \
	$(API_OBJS) \
	src/eglGetProcAddress.o

#	glGenLists.o
#	glIsList.o
#	glDeleteLists.o

libGLU.a_OBJS = \
	src/GLU/gluBuildMipmaps.o \
	src/GLU/gluLookAt.o \
	src/GLU/gluLookAtf.o \
	src/GLU/gluPerspective.o \
	src/GLU/gluPerspectivef.o \
	src/GLU/gluScaleImage.o \
	src/GLU/pspglu.o

libglut.a_OBJS = \
	src/glut/glut.o

# Move object files elsewhere for cleanliness
API_OBJS_OUT = $(patsubst src/%.o, obj/%.o, $(API_OBJS))
libGL.a_OBJS_OUT = $(patsubst src/%.o, obj/%.o, $(libGL.a_OBJS))
libGLU.a_OBJS_OUT = $(patsubst src/%.o, obj/%.o, $(libGLU.a_OBJS))
libglut.a_OBJS_OUT = $(patsubst src/%.o, obj/%.o, $(libglut.a_OBJS))

all: $(DEPDIR) $(libGL.a_OBJS_OUT_OUT) $(libGLU.a_OBJS_OUT) $(libglut.a_OBJS_OUT) libGL.a libGLU.a libglut.a

%.a: $(libGL.a_OBJS_OUT) $(libGLU.a_OBJS_OUT) $(libglut.a_OBJS_OUT)
	$(RM) $@
	$(AR) cru $@ $($@_OBJS_OUT)
	$(RANLIB) $@
	@$(ARCH)nm -o -fp -g --defined-only $@ | \
		awk '$$2~/^(gl|egl|glut|__pspgl)/ { next } \
				{ if (!bad) print "Bad symbols:"; print "\t", $$1, $$2; bad++ } \
			END	{ if (bad) { \
					print bad," bad symbol(s)"; exit(1) \
				  } else { \
					print "Namespace OK" } \
				}'


obj/eglGetProcAddress.o: src/eglGetProcAddress.c pspgl_proctable.h

# Extract all the public GL and EGL API symbols which are extensions (ends with PSP, ARB or EXT)
# Symbols must be sorted by name so that bsearch can be used to look for them.
pspgl_proctable.h: $(API_OBJS_OUT) Makefile
	$(ARCH)nm -fp -g --defined-only $(API_OBJS_OUT) | sort -k1 | \
		awk '$$2=="T" && $$1 ~ /^(gl|egl)[A-Z][a-zA-Z]+(PSP|ARB|EXT)/ \
			{ print "\t{ \"" $$1 "\", (void (*)())"$$1 " }," }' > $@ \
			|| rm -f $@

$(DEPDIR):
	mkdir $(DEPDIR)

obj/%.o: src/%.c
	@mkdir -p $(@D)
	@mkdir -p $(dir $(DEPDIR)/$*.d)
	$(CC) $(CFLAGS) -MD -MF $(DEPDIR)/$*.d $< -c -o $@

src/*.S.o:
	$(CC) $(CFLAGS) -c $<

tar: clean
	( cd .. && tar cvfz pspgl-`date "+%Y-%m-%d"`.tar.gz pspgl --exclude "*.DS_Store" && cd - )

clean:
	$(RM) -rf obj/*.o *.a $(DEPDIR) pspgl_proctable.h
	$(MAKE) -C tools clean
	$(MAKE) -C tests clean
	$(MAKE) -C test-q3 clean
	$(MAKE) -C test-vfpu clean

install: all
	mkdir -p $(PSPPATH)/include $(PSPPATH)/lib
	mkdir -p $(PSPPATH)/include/GL $(PSPPATH)/include/GLES
	cp GL/*.h $(PSPPATH)/include/GL
	cp GLES/*.h $(PSPPATH)/include/GLES
	cp libGL.a $(PSPPATH)/lib
	cp libGLU.a $(PSPPATH)/lib
	cp libglut.a $(PSPPATH)/lib

-include $(wildcard $(DEPDIR)/*.d) dummy

